import numpy as np
from tqdm import tqdm
import os
import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray
from uproot_methods.classes.TVector3 import TVector3Array

def mid(bins):
    return (bins[1]-bins[0])*0.5+bins[:-1]

def load(fnames, columns, tree):
    files_in = []
    for fidx in range(len(fnames)):
        print(os.path.basename(fnames[fidx]))
        files_in.append(uproot.open(fnames[fidx]))
        count = 0
        for cluster in tqdm(list(files_in[fidx][tree].clusters())):
            if count == 0 and fidx == 0:
                arr = files_in[fidx][tree].pandas.df(columns,
                                                    entrystart=cluster[0], entrystop=cluster[1])
            else:
                arr = arr.append(files_in[fidx][tree].pandas.df(columns,
                                              entrystart=cluster[0], entrystop=cluster[1]), ignore_index=True)
            count+=1
            
    return arr

def weighted_err(var, weights, bins):
    return np.array([np.sqrt(np.sum(weights[(var>=bins[i])&(var<bins[i+1])]**2)) 
                     for i in range(len(bins)-1)])

def calc_bstrap(arr, col, yr, norm, norm_IQR, bins):
    n2,_ = np.histogram(arr[arr['ntag']==2][col],
                        weights=arr[arr['ntag']==2]['NN_d24_weight_bstrap_med_%s'%yr]*norm,
                        bins=bins)
        
    n_IQRup,_ = np.histogram(arr[arr['ntag']==2][col],
                              weights=(arr[arr['ntag']==2]['NN_d24_weight_bstrap_med_%s'%yr]
                                       +arr[arr['ntag']==2]['NN_d24_weight_bstrap_IQR_%s'%yr]/2.),
                              bins=bins)

    return abs(n_IQRup*np.sum(n2)/np.sum(n_IQRup)+n2*norm_IQR/2.-n2)

def systs(arr, norm, norm_VR, yr, bins, col= 'm_hh_cor', var='HT', cutoff=300):
    if var in arr.keys():
        var_arr = arr[var]
    else:
        if var == 'HT':
            var_arr = arr['pT_h1_j1'] + arr['pT_h1_j2'] + arr['pT_h2_j1'] + arr['pT_h2_j2']
        else:
            print("Not found!")
            return 0
    
    n_back_low, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr< cutoff)][col], bins=bins,
                            weights=arr[(arr['ntag']==2)& (var_arr< cutoff)]['NN_d24_weight_bstrap_med_%s'%yr]*norm)
    
    n_back_high, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr >= cutoff)][col], bins=bins,
                        weights=arr[(arr['ntag']==2)& (var_arr >= cutoff)]['NN_d24_weight_bstrap_med_%s'%yr]*norm)
        
    n_back_low_VR, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr< cutoff)][col], bins=bins,
                            weights=arr[(arr['ntag']==2)& (var_arr< cutoff)]['NN_d24_weight_VRderiv_bstrap_med_%s'%yr]*norm_VR)
    
    n_back_high_VR, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr >= cutoff)][col], bins=bins,
                        weights=arr[(arr['ntag']==2)& (var_arr >= cutoff)]['NN_d24_weight_VRderiv_bstrap_med_%s'%yr]*norm_VR)
    
    
    lowVRw = n_back_low_VR + n_back_high
    highVRw = n_back_low + n_back_high_VR
    nom = n_back_low + n_back_high
    
    lowVRi = 2*nom - lowVRw
    highVRi = 2*nom - highVRw
    
    return nom, lowVRw, highVRw, lowVRi, highVRi


def cosThetaStar(df):
    '''
    Get the helicity angles for the df made from RR trees
    '''
    
    '''
    HC 4 vectors
    '''
    lab_hc1 = TLorentzVectorArray.from_ptetaphim(df.pT_h1.values, 
                                                 df.eta_h1.values, 
                                                 df.phi_h1.values, 
                                                 df.m_h1.values)
    lab_hc2 = TLorentzVectorArray.from_ptetaphim(df.pT_h2.values, 
                                                 df.eta_h2.values, 
                                                 df.phi_h2.values, 
                                                 df.m_h2.values) 
    
    lab_HH = lab_hc1 + lab_hc2

    boost = - lab_HH.boostp3

    rest_hc1 = lab_hc1._to_cartesian().boost(boost)
    rest_hc2 = lab_hc1._to_cartesian().boost(boost)
    
    return abs(np.cos(rest_hc1.theta))

