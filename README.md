# Non-resonant Studies

This repository is mostly useful for the pyhf limit setting code. The driving script is `run_limits.py`, which 
can run limits for both Nicole's format (converted from hdf5 to ROOT with `h5_to_root.py`) and standard 
resolved-recon NNT's.

Limit functionality includes options for categories and $`\kappa_{\lambda}`$ reweighting, as well as knobs to tune some of 
the parameters and export workspaces in both pyhf json and ROOT formats. The $`\kappa_{\lambda}`$ reweighting 
itself is done with a function in `kl_rw.py` (thanks to Nicole!)

``` console
$ ./run_limits.py -h
usage: run_limits.py [-h] [-i INPUT_DIR] [-d DATA_FILE [DATA_FILE ...]]
                     [-s SIG_FILE [SIG_FILE ...]] [-y YEARS [YEARS ...]]
                     [--stat-only] [--no-bootstrap] [--format IN_FORMAT]
                     [--HTcut HTCUT] [-l LABEL] [--categorize CAT_VAR]
                     [--cat-edges CAT_BOUNDS [CAT_BOUNDS ...]]
                     [--bins BINS [BINS ...]]
                     [--range BIN_RANGE [BIN_RANGE ...]] [--var VAR]
                     [--kl KL [KL ...]] [--save-wkspace SAVE_WS]
                     [--wkspace-only] [--doTheoryLumiUnc]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir INPUT_DIR
                        Input directory
  -d DATA_FILE [DATA_FILE ...], --data DATA_FILE [DATA_FILE ...]
                        Input data filenames
  -s SIG_FILE [SIG_FILE ...], --signal SIG_FILE [SIG_FILE ...]
                        Input signal filenames
  -y YEARS [YEARS ...], --years YEARS [YEARS ...]
                        Input years
  --stat-only           Stat only limits
  --no-bootstrap        No bootstrap error
  --format IN_FORMAT    Nicole's format or resolved-recon
  --HTcut HTCUT         HT (or other syst) cutoff
  -l LABEL, --label LABEL
                        Label for output
  --categorize CAT_VAR  Variable to categorize in (e.g. cosThetaStar)
  --cat-edges CAT_BOUNDS [CAT_BOUNDS ...]
                        Category boundaries
  --bins BINS [BINS ...]
                        Bins for m_hh_cor. Single arg = n_bins, list = bin
                        edges.
  --range BIN_RANGE [BIN_RANGE ...]
                        Lower and upper bounds for m_hh_cor hist
  --var VAR             Variable on which to run limits
  --kl KL [KL ...]      kappa lambda value(s)
  --save-wkspace SAVE_WS
                        Save workspace if given output name
  --wkspace-only        Only make workspace (don't run limits)
  --doTheoryLumiUnc     Do theory+lumi uncertainty
```


Most of the heavy lifting is done in `lim_utils.py`, which has classes for data prep 
and mu finding (similar in spirit to the old ROOT script). The default setup is unbounded 
POI with $`q_{\mu}`$ test statistic. Error calculation functions are in `utils.py`.

A script to run pulls and impacts is in progress (`run_pulls.py` and `pull_imp_utils.py`), but 
some work is still needed.

An example for stat-only limits without bootstrap uncertainties for the MAY20-1 production for 2016 only is
``` console
$ ./run_limits.py -d /eos/user/s/sgasioro/public/central_prod/NNT_MAY20-1/data16_NN_100_bootstraps_IQR.root -s /eos/user/m/mswiatlo/bbbb/NNT_MAY20-1/MC/Fullsim/450000_mc16a/nominal.root -y 16 --format resolved-recon --stat-only --no-bootstrap --label BDT-MAY20-1
```

Multiple years can be fit together by just adding to the list of input data (`-d`) and signal (`-s`)file names, 
as well as to the list of years (`-y`).

For exporting workspaces, if `--save-wkspace` is given a file with extension `.root`, it'll do the export to ROOT, otherwise it'll do json.


To collect kl limits into one csv, use `collect_kl_lims.py` with a wildcard in the position of the kl string, e.g.
``` console
$ ./collect_kl_lims.py --pattern exp-lim-smnr-stat-only-16-\*-BDT-JUN20-comb.csv
```

By default, this returns limits on $`\mu`$, but the `--with-xs` flag multiplies through the cross sections ($`\sigma(pp\rightarrow HH)`$, no 4b BR included).
