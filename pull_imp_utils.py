from lim_utils import *
import pyhf
import numpy as np
from itertools import product

def fitresults(ws, constraints=None, return_result_obj=False):
    model = ws.model(
                 measurement_name="Measurement",
                 modifier_settings={
                        "normsys": {"interpcode": "code4"},
                        "histosys": {"interpcode": "code4p"},
                  },
               )

    data = ws.data(model)

    pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer(verbose=True))
    
    constraints = constraints or []
    init_pars = model.config.suggested_init()
    fixed_params = model.config.suggested_fixed()
    
    for idx,fixed_val in constraints:
        init_pars[idx] = fixed_val
        fixed_params[idx] = True
    
    if return_result_obj:
        result, obj = pyhf.infer.mle.fit(
            data, model,
            par_bounds=[(-40,40)]*len(model.config.suggested_bounds()),
            init_pars=init_pars, fixed_params=fixed_params, return_uncertainties=True,
            return_result_obj=True
        )
        bestfit = result[:, 0]
        errors = result[:, 1]
        return model, data, bestfit, errors, obj

    else:
        result = pyhf.infer.mle.fit(
            data, model, 
            par_bounds=[(-40,40)]*len(model.config.suggested_bounds()),
            init_pars=init_pars, fixed_params=fixed_params, return_uncertainties=True
        )
        bestfit = result[:, 0]
        errors = result[:, 1]
        return model, data, bestfit, errors

def calc_pulls(ws):
    model, data, bestfit, errors = fitresults(ws)
    pulls = pyhf.tensorlib.concatenate(
        [
            (bestfit[model.config.par_slice(k)] - model.config.param_set(k).suggested_init)
            / model.config.param_set(k).width()
            for k in model.config.par_order
            if model.config.param_set(k).constrained
        ]
    )
    pullerr = pyhf.tensorlib.concatenate(
        [
            errors[model.config.par_slice(k)] / model.config.param_set(k).width()
            for k in model.config.par_order
            if model.config.param_set(k).constrained
        ]
    )
    labels = np.asarray(
        [
            "{}[{}]".format(k, i) if model.config.param_set(k).n_parameters > 1 else k
            for k in model.config.par_order
            if model.config.param_set(k).constrained
            for i in range(model.config.param_set(k).n_parameters)
        ]
    )

    order = np.argsort(labels)
    fitted = bestfit[order]
    fiterr = errors[order]
    labels = labels[order]
    pulls = pulls[order]
    pullerr = pullerr[order]

    return pulls, pullerr, labels

def calc_impact(idx, b, e, width, poi_index, ws):
    print(idx, b, e)
    _, _, bb, ee = fitresults(ws, constraints=[(idx, b - e)])
    poi_dn_post = bb[poi_index]
    
    _, _, bb, ee = fitresults(ws, constraints=[(idx, b + e)])
    poi_up_post = bb[poi_index]

    _, _, bb, ee = fitresults(ws, constraints=[(idx, b - width)])
    poi_dn_pre = bb[poi_index]
    
    _, _, bb, ee = fitresults(ws, constraints=[(idx, b + width)])
    poi_up_pre = bb[poi_index]

    return np.asarray([poi_dn_post, poi_up_post, poi_dn_pre, poi_up_pre])

def get_impact_data(ws):
    model, _, b, e = fitresults(ws)
    
    widths = pyhf.tensorlib.concatenate(
        [
            model.config.param_set(k).width()
            for k, v in model.config.par_map.items()
            if model.config.param_set(k).constrained
        ]
    )
    labels = np.asarray(
        [
            "{}[{:02}]".format(k, i)
            if model.config.param_set(k).n_parameters > 1
            else k
            for k in model.config.par_order
            if model.config.param_set(k).constrained
            for i in range(model.config.param_set(k).n_parameters)
        ]
    )
   
    idxs = np.arange(len(b))
    idxs = np.delete(idxs, model.config.poi_index)

    poi_free = b[model.config.poi_index]
    impacts = []
    imp_labels = []

    for i, width in enumerate(widths):
        print(width)
        idx = idxs[i]
        impct = calc_impact(idx, b[idx], e[idx], width, model.config.poi_index, ws)
        impacts.append(impct - poi_free)
        imp_labels.append(labels[i])


    return np.asarray(impacts), np.array(imp_labels)

def get_corr_matrix(ws):
    model, data, bestfit, errors, obj = fitresults(ws, constraints=None, return_result_obj=True)
    full_corr_matrix = obj.minuit.matrix(correlation=True, skip_fixed=False)
    
    labels = np.asarray(
        [   
            "{}[{}]".format(k, i) if model.config.param_set(k).n_parameters > 1 else k
            for k in model.config.par_order
            for i in range(model.config.param_set(k).n_parameters)
        ])

    idxs = np.where([(label.find('uncorr') == -1) and label != 'Theory' and label != 'lumi'
                  for label in labels])[0]


    full_idxs = np.array(list(product(idxs, idxs)))
    sel_matrix = (np.asarray(full_corr_matrix)[full_idxs[:,0], full_idxs[:,1]]).reshape(len(idxs),len(idxs))

    return sel_matrix, labels[idxs]

